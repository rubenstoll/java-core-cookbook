public class Strings {

    public boolean isEqual(String s1, String s2) {

        return s1 == s2;
    }

    public boolean isEqualInternal(String s1, String s2) {

        return s1 == s2.intern();
    }


}
