package interfaces.jenkov;

//http://tutorials.jenkov.com/java/interfaces.html
public interface MyInterface {

  public String hello = "Hello";

  public void sayHello();

}
