package functional.lambdas.dzone;

import java.util.Arrays;
import java.util.IntSummaryStatistics;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * https://dzone.com/articles/using-java-collectors
 */
public class StreamsAndCollectors {

  public static Integer summing() {

    List<Integer> numbers = Arrays.asList(9, 5, 66, 56, 4, 8, 9);
    // int sum = numbers.stream().reduce(0, (x, y) -> x + y);

    int sum = numbers.stream().collect(Collectors.summingInt(x -> x));

    return sum;
  }

  public static double average(List<Integer> numbers) {
    return numbers.stream().collect(Collectors.averagingInt(x -> x));
  }

  public static double maxMin(List<Integer> numbers) {
    IntSummaryStatistics r = numbers.stream().collect(Collectors.summarizingInt(x -> x));
    return numbers.stream().collect(Collectors.averagingInt(x -> x));
  }

  public static void partition(List<Integer> numbers) {

    Map<Boolean, List<Integer>> parts = numbers.stream().collect(Collectors.partitioningBy(x -> x > 50));
    System.out.println(numbers + " =>\n" + "   true: " + parts.get(true) + "\n" + "  false: " + parts.get(false) + "\n");

  }

}
