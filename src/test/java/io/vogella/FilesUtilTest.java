package io.vogella;

import org.hamcrest.Matchers;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;

import java.io.File;
import java.util.List;
import java.util.Objects;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.empty;
import static org.hamcrest.core.IsNot.not;
import static org.hamcrest.core.IsNull.nullValue;

/**
 *
 */
public class FilesUtilTest {


    private String filename;

    @BeforeEach
    public void setUp() throws Exception {

        this.filename = "file.txt";

    }

    @Test
    public void readTextFile() throws Exception {

        String readInFile = FilesUtil.readTextFile(filename);

        assertThat(readInFile, is(not(nullValue())));

    }

    @Test
    public void readTextFileByLines() throws Exception {

        ClassLoader classLoader = getClass().getClassLoader();
//    File file = new File(classLoader.getResource(this.filename).getFile());
        File file = new File(Objects.requireNonNull(classLoader.getResource(this.filename)).getFile());

        List<String> lines = FilesUtil.readTextFileByLines(file.getAbsolutePath());
        assertThat(lines, Matchers.not(empty()));

    }

    @Disabled
    @Test
    public void writeToTextFile() throws Exception {

    }

    @Disabled
    @AfterEach
    public void tearDown() throws Exception {


    }
}