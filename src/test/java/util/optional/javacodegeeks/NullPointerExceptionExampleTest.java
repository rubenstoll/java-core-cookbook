package util.optional.javacodegeeks;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

public class NullPointerExceptionExampleTest {

    @BeforeEach
    public void setUp() throws Exception {
    }

    @AfterEach
    public void tearDown() throws Exception {
    }

    @Test
    public void examples() {

        NullPointerExceptionExample.typicalNullPointer();
        NullPointerExceptionExample.emptyOptionalCreation();
        NullPointerExceptionExample.nonEmptyOptional();
        NullPointerExceptionExample.nullableOptional();
        NullPointerExceptionExample.getExample();
        NullPointerExceptionExample.orElseExample();
        NullPointerExceptionExample.orElseThrowExample();
        NullPointerExceptionExample.isPresentExample();
        NullPointerExceptionExample.ifPresentExample();
        NullPointerExceptionExample.filterExample();
        NullPointerExceptionExample.mapExample();

    }
}