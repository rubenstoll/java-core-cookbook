package util.collections;

import functional.lambdas.misc.LambdaTopTen;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.List;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.containsInAnyOrder;


public class ConversionUtilsTest {

  static Logger LOGGER = LoggerFactory.getLogger(LambdaTopTen.class);

  @BeforeEach
  public void setUp() throws Exception {
  }

  @AfterEach
  public void tearDown() throws Exception {
  }

  @Test
  public void convertArrayToListJava8() {
    String[] names = new String[]{"pedro","john","stacey"};
    List<String> convertedList = ConversionUtils.convertArrayToListJava8(names);
    convertedList.forEach(n -> LOGGER.info(n));

    assertThat(convertedList, containsInAnyOrder("stacey","pedro","john"));

  }
}