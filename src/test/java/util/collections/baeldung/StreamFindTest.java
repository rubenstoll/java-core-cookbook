package util.collections.baeldung;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.Optional;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.junit.jupiter.api.Assertions.assertTrue;

public class StreamFindTest {

    @BeforeEach
    public void setUp() throws Exception {
    }

    @Test
    public void streamFindFirst() {

        Optional<String> result = StreamFind.streamFindFirst();

        assertTrue(result.isPresent());
        assertThat(result.get(), is("A"));
    }

    @Test
    public void findAny() {

        Optional<String> result = StreamFind.streamFindAny();
        assertTrue(result.isPresent());
//    assertThat(result.get(), anyOf(is("A"), is("B"), is("C"), is("D")));

    }
}