import org.junit.jupiter.api.Test;

class StringsTest {

    @Test
    void isEqual() {

        Strings strings = new Strings();
        String s1 = "java";
        String s2 = new String("java");

        strings.isEqual(s1, s2);
        strings.isEqualInternal(s1,s2);

    }
}