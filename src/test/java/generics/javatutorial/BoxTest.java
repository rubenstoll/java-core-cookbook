package generics.javatutorial;


import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import generics.oracle.Box;

/**
 * one line comment with period.
 */
public class BoxTest {

  private static Logger LOGGER = LoggerFactory.getLogger(BoxTest.class);

  private Box<Integer> integerBox;

  @BeforeEach
  public void setUp() throws Exception {
    this.integerBox = new Box<Integer>();
  }

  @Test
  public void setAndGet() throws Exception {
    integerBox.set(5);
    LOGGER.debug("get value from generic box: " + integerBox.get());
  }

}