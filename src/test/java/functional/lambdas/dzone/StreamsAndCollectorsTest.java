package functional.lambdas.dzone;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.equalTo;


public class StreamsAndCollectorsTest {

    @BeforeEach
    public void setUp() throws Exception {
    }


    @Disabled
    @Test
    public void summing() {

        int sum = StreamsAndCollectors.summing();
        assertThat(sum, equalTo(544));

    }

    @Disabled
    @Test
    public void average() {
    }

    @Disabled
    @Test
    public void maxMin() {
    }

    @Disabled
    @Test
    public void partition() {
    }
}